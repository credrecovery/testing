# My project's README


To run the tests from the command line interface, follow the below steps
1. Build the solution
2. Run nunit3-console.exe with the parameter as the project.dll
Example:-C:\CRS\testing\CRSAutomationTests\CRSAutomationTests\bin\Debug>c:\CRS\testing\CRSAutomationTests\packages\NUnit.ConsoleRunner.3.4.1\tools\nunit3-console.exe CRSAutomationTests.dll


The result will be like as below
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

C:\CRS\testing\CRSAutomationTests\CRSAutomationTests\bin\Debug>c:\CRS\testing\CRSAutomationTests\packages\NUnit.ConsoleRunner.3.4.1\tools\nunit3-console.exe CRSAutomationTests.dll
NUnit Console Runner 3.4.1
Copyright (C) 2016 Charlie Poole

Runtime Environment
   OS Version: Microsoft Windows NT 6.1.7601 Service Pack 1
  CLR Version: 4.0.30319.42000

Test Files
    CRSAutomationTests.dll

Starting ChromeDriver 2.25.426923 (0390b88869384d6eb0d5d09729679f934aab9eed) on
port 46793
Only local connections are allowed.
=> CRSAutomationTests.Features.ForgotPasswordFeature.ForgotPassword
Given I am on the Casino Betway Page
-> done: ForgotPasswordSteps.GivenIAmOnTheCasinoBetwayPage() (1.6s)
When I click the ForgotPassword link
-> done: ForgotPasswordSteps.WhenIClickTheForgotPasswordLink() (6.3s)
Then I can see the forgot password page
-> done: ForgotPasswordSteps.ThenICanSeeTheForgotPasswordPage() (0.0s)

Run Settings
    WorkDirectory: C:\CRS\testing\CRSAutomationTests\CRSAutomationTests\bin\Debu
g
    ImageRuntimeVersion: 4.0.30319
    ImageTargetFrameworkName: .NETFramework,Version=v4.5.2
    ImageRequiresX86: False
    ImageRequiresDefaultAppDomainAssemblyResolver: False
    NumberOfTestWorkers: 4

Test Run Summary
  Overall result: Passed
  Test Count: 1, Passed: 1, Failed: 0, Inconclusive: 0, Skipped: 0
  Start time: 2017-02-07 15:20:27Z
    End time: 2017-02-07 15:20:47Z
    Duration: 19.624 seconds

Results (nunit3) saved as TestResult.xml
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


To run the tests from the GUI, follow the steps
1. Navigate NUnit.Runners tools folders and double-click "nunit.exe" 
Ex:- C:\Users\Deepthi.Kolla\.nuget\packages\NUnit.Runners\2.6.0.12051\tools\nunit.exe
2. Open the project.dll from the File menu
Ex:- File->Open Project and select the dll of your project
3. Click on the "Run" button


To run the tests with the specific tags from the command line interface, follow the below steps
1. Build the solution
2. Run nunit3-console.exe with the parameter as the project.dll and the option "where" with the category as "tagname"
Example:-C:\CRS\testing\CRSAutomationTests\CRSAutomationTests\bin\Debug>c:\CRS\testing\CRSAutomationTests\packages\NUnit.ConsoleRunner.3.4.1\tools\nunit3-console.exe CRSAutomationTests.dll --where "cat=smoke"