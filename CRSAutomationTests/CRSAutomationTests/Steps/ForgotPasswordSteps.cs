﻿using TechTalk.SpecFlow;
using OpenQA.Selenium;
using CRSAutomationTests.Pages;
using Kraken.Tests;
using CRSAutomationTests.Base;
using CRSAutomationTests.Elements;
using System;

namespace CRSAutomationTests.Steps
{
    [Binding]
    public class ForgotPasswordSteps : Hooks
    {
        private CrsBasePage crsBasePage => new CrsBasePage(Driver);

        [Given(@"I am on the Casino Betway Page")]
        public void GivenIAmOnTheCasinoBetwayPage()
        {
            crsBasePage.urlsPage.NavigateToCasinoBetwayPage();
        }

        [Given(@"I am on the Sports Betway Page")]
        public void GivenIAmOnTheSportsBetwayPage()
        {
            crsBasePage.urlsPage.NavigateToSportsBetwayPage();
        }

        [When(@"I click the ForgotPassword link for casino page")]
        public void WhenIClickTheForgotPasswordLinkForCasinoPage()
        {
            crsBasePage.Click.Perform(crsBasePage.forgotPasswordElements.ForgotPasswordLinkForCasino);
        }

        [When(@"I click the ForgotPassword link for sports page")]
        public void WhenIClickTheForgotPasswordLinkForSportsPage()
        {
            crsBasePage.Click.Perform(crsBasePage.forgotPasswordElements.ForgotPasswordLinkForSports);
        }

        [Then(@"I can see the forgot password page")]
        public void ThenICanSeeTheForgotPasswordPage()
        {
            crsBasePage.ElementText.Equals(crsBasePage.forgotPasswordElements.userInformation);
        }
    }
}
