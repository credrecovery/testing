﻿Feature: ForgotPassword
	In order to reset my password
	I want to access my forgot password link

@mytag
Scenario: Forgot Password for betCasinoWay
	Given I am on the Casino Betway Page
	When I click the ForgotPassword link for casino page
	Then I can see the forgot password page

@smoke
Scenario: Forgot Password for betSportsWay
	Given I am on the Sports Betway Page
	When I click the ForgotPassword link for sports page
	Then I can see the forgot password page