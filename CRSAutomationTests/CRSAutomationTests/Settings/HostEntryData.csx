﻿Add("IntegrationHost", new Dictionary<string, string>
				 {

	 
				 });
Add("TestHost", new Dictionary<string, string>
				 {
					{"atreyu","10.10.3.61"},
					{"betwaybackoffice","10.10.3.61"},
					{"www.betwaypartners.com","10.10.3.68"},
					{"www.betwaypartners.es","10.10.3.69"},
					{"www.betwaypartners.it","10.10.3.70"},
					{"www.buffalopartners.com","10.10.3.72"},
					{"www.hippodromeaffiliates.com","10.10.3.66"},
					{"dms","10.10.3.113"},
					{"dms.api.authorise","10.10.3.113"},
					{"dms.api.resource","10.10.3.113"},
					{"casino.betway.com", "10.10.3.17"},
				    {"plus.betway.com", "10.10.3.17"},
					{"vegas.betway.com", "10.10.3.17"},
					{"poker.betway.com", "10.10.3.17"},
					{"betway.com", "10.10.3.17"},
					{"esports.betway.com", "10.10.3.17"},
					{"blog.betway.com", "10.10.3.92"},
					{"www.betway.de", "10.10.3.55"},
					{"www.betway.dk", "10.10.3.56"},
					{"betway.es", "10.10.3.140"},
					{"www.betway.es", "10.10.3.140"},
					{"promo.betway.es", "10.10.3.35"},
					{"m.betway.es", "10.250.251.5"},
					{"www.betway.it", "10.10.3.57"},
					{"m.betway.it", "10.10.3.54"},
					{"casino.betway.be", "10.10.3.140"},
					{"betway.be", "10.10.3.140"},
					{"spincasino.co.uk", "10.10.3.78"},
					{"spincasino.it", "10.10.3.93"},
					{"www.hippodromeonline.com", "10.10.3.128"},
					{"hippodromeDB", "10.200.2.13"},
					{"images.thebetwaygroup.com", "10.10.3.78"}
				 });

Add("TestRefreshHost", new Dictionary<string, string>
				 {
					{"betway.es", "10.10.3.146"},
					{"www.betway.es", "10.10.3.146"},
					{"casino.betway.be", "10.10.3.146"},
					{"betway.be", "10.10.3.146"}
				 });


Add("UatHost", new Dictionary<string, string>
				 {
					{"atreyu","10.250.253.74"},
					{"betwaybackoffice","10.250.253.74"},
					{"www.betwaypartners.com betwaypartners.com","10.250.254.75"},
					{"www.betwaypartners.es betwaypartners.es","10.250.254.76"},
					{"www.betwaypartners.it betwaypartners.it","10.250.254.77"},
					{"www.buffalopartners.com buffalopartners.com","10.250.253.71"},
					{"www.hippodromeaffiliates.com hippodromeaffiliates.com","10.250.253.72"},
					{"casino.betway.com","10.250.251.50"},
					{"vegas.betway.com","10.250.251.50"},
					{"poker.betway.com","10.250.251.50"},
					{"betway.com","10.250.251.50"},
					{"esports.betway.com","10.250.251.53"},
					{"blog.betway.com","10.250.254.81"},
					{"www.betway.de","10.250.251.2"},
					{"www.betway.dk","10.250.251.19"},
					{"m.betway.es","10.250.251.5"},
					{"www.betway.it","10.250.251.24"},
					{"m.betway.it","10.250.251.30"},
					{"spincasino.co.uk","10.250.252.67"},
					{"spincasino.it","10.250.254.82"}
				 });
//used for node1				 
Add("LiveBlueHost", new Dictionary<string, string>
				 {
					{"betway.es www.betway.es", "10.249.14.21"},
					{"betway.be www.betway.be casino.betway.be", "10.249.14.5"}
				 });
//used for node 2
Add("LiveBlueHost1", new Dictionary<string, string>
				 {
					{"betway.es www.betway.es", "10.249.14.22"},
					{"betway.be www.betway.be casino.betway.be", "10.249.14.7"}
				 });
//used for node 3
Add("LiveGreenHost", new Dictionary<string, string>
				 {
					{"betway.es www.betway.es", "10.249.14.23"},
					{"betway.be www.betway.be casino.betway.be", "10.249.14.9"}
				 });
//used for node 4
Add("LiveGreenHost1", new Dictionary<string, string>
				 {
					{"betway.es www.betway.es", "10.249.14.20"},
					{"betway.be www.betway.be casino.betway.be", "10.249.14.19"}
				 });

				 
Add("lobbyEproc01", new Dictionary<string, string>
				 {
					{"casino.betway.com", "10.249.60.50"},
					{"vegas.betway.com", "10.249.60.50"},
					{"plus.betway.com", "10.249.60.50"},
			
				 });
Add("lobbyEproc02", new Dictionary<string, string>
				 {
					{"casino.betway.com", "10.249.60.51"},
					{"vegas.betway.com", "10.249.60.51"},
					{"plus.betway.com", "10.249.60.51"},

				 });

Add("lobbyEproc05", new Dictionary<string, string>
				 {
					{"casino.betway.com", "10.249.60.64"},
					{"vegas.betway.com", "10.249.60.64"},
					{"plus.betway.com", "10.249.60.64"},
		
				 });

Add("lobbyEproc03", new Dictionary<string, string>
				 {
					{"casino.betway.com", "10.249.60.52"},
					{"vegas.betway.com", "10.249.60.52"},
					{"plus.betway.com", "10.249.60.52"},
	
				 });