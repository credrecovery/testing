﻿#r "Kraken.dll"
#load "HostEntryData.csx"

using Kraken;


var baseUrl = "http://somethhing";
Add("Browser", "chrome");
Add("Page", baseUrl + "something");


//Selenium Grid url used for remote 
Add("GridUrl", "http://ondemand.saucelabs.com:80/wd/hub");

//Capabilites for remote webdriver, set browser to Remote
Dictionary<string, object> setup = new Dictionary<string, object>
				 {
					 //{"platform", "Windows 8"},
					 {"username", "lbmauto"},
					 {"accesskey", "9f5cb3e0-4f2c-40ad-8ca5-018597865dfe"},
					 {"screen-resolution", "1024x768"},
					 {"version", "40.0"},
					 {"browserName", "chrome"},
					 {"name", "TEST"},
					 {"tunnelIdentifier", "kraken1"},
					 {"maxDuration", 10800}, 
					 {"idleTimeout", 200 }
					// {"passed", "true"}
				
				 };

//Capabilites mobile
Dictionary<string, object> setupmobile = new Dictionary<string, object>
				 {
					 //{"platform", "Windows 8"},
					 //{"username", "lbmauto"},
					 //{"accesskey", "9f5cb3e0-4f2c-40ad-8ca5-018597865dfe"},
					 //{"screen-resolution", "1024x768"},
					 //{"version", "40.0"},
					  {"browserName", "chrome"},
					 
                      //{"name", "TEST"},
					 //{"tunnelIdentifier", "kraken1"},
					 //{"maxDuration", 10800}, 
					 //{"idleTimeout", 200 }
					// {"passed", "true"}
				
				 };
Add("resolution", "default");
Add("screenwidth", "1280");
Add("screenheight", "960");
				 					 
Add("Capabilities", setup);
Add("CapabilitiesMobile", setup);

// To edit Host Entry Test, Uat1, Uat2, Live
Add("Host", "");

//To edit Tracking Environment for SQL database Test, Uat
Add("Environment", "Test");

//select platform
Add("platform", "Windows 10");

//select device
Add("device", "Samsung Galaxy S4");

Add("BrowserMobile", "Apple iPhone 6");


//To load other settings files in one
//Load("Data.csx");