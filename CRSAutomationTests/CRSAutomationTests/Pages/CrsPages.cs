﻿using Kraken.PageObject;
using CRSAutomationTests.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace CRSAutomationTests.Pages
{
    public class CrsPages : CrsBasePage
    {
        public CrsBasePage crsBasePage => new CrsBasePage(_driver);

        public CrsPages(IWebDriver browser) : base(browser)
        {
            _driver = browser;
            PageFactory.InitElements(_driver, this);
        }
    }
}
