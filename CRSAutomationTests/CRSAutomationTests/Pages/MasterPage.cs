﻿using Kraken.Tests;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace CRSAutomationTests.Pages
{
    public class CrsTest : BaseTest
    {
        public CrsPages CrsPages
        {
            get { return new CrsPages(Driver); }
        }
    }
}
