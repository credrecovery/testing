﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using Kraken.PageObject;
using CRSAutomationTests.Base;
using OpenQA.Selenium.Support.PageObjects;

namespace CRSAutomationTests.Pages
{
    public class UrlsPage : CrsBasePage
    {
        public CrsBasePage crsBasePage => new CrsBasePage(_driver);
        public UrlsPage(IWebDriver browser) : base(browser)
        {
            _driver = browser;
        }

        public void NavigateToCasinoBetwayPage()
        {
            Navigate.GoTo("https://casino.betway.com");
        }

        public void NavigateToSportsBetwayPage()
        {
            Navigate.GoTo("https://sports.betway.com/");
        }
    }
}
