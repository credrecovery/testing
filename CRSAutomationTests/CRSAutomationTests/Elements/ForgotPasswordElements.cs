﻿using CRSAutomationTests.Base;
using CRSAutomationTests.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace CRSAutomationTests.Elements
{

    class ForgotPasswordElements : CrsBasePage
    {
        public ForgotPasswordElements(IWebDriver browser) : base(browser)
        {
            _driver = browser;
        }

        [FindsBy(How = How.XPath, Using = "/html/body/header/div/div/section/section[1]/form/div[1]/div[2]/label")]
        public IWebElement ForgotPasswordLinkForCasino { get; set; }

        [FindsBy(How = How.Id, Using = "loginForgotPassword")]
        public IWebElement ForgotPasswordLinkForSports { get; set; }

        [FindsBy(How = How.ClassName, Using = "page_title")]
        public IWebElement userInformation { get; set; }
    }
}
