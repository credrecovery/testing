﻿using CRSAutomationTests.Pages;
using Kraken.PageObject;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using CRSAutomationTests.Elements;

namespace CRSAutomationTests.Base
{
    public class CrsBasePage : BasePage
    {
        protected IWebDriver _driver;

        public CrsBasePage(IWebDriver browser) : base(browser)
        {
            _driver = browser;
            PageFactory.InitElements(_driver, this);
        }

        #region Pages
        internal CrsPages crsPage => new CrsPages(_driver);
        internal UrlsPage urlsPage => new UrlsPage(_driver);
        #endregion

        #region Elements
        internal ForgotPasswordElements forgotPasswordElements => new ForgotPasswordElements(_driver);
        #endregion
    }
}
